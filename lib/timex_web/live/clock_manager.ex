defmodule TimexWeb.ClockManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {_, now} = :calendar.local_time()

    Process.send_after(self(), :working, 1000)
    {:ok, %{ui_pid: ui, mode: Time, st1: Working, st2: Idle, time: Time.from_erl!(now), alarm_time: Time.new!(0,0,0), count: 0, show: true, selection: Hour, timer: nil}}
  end

  def handle_info(:"top-left", %{mode: Time, st1: Working, st2: Idle} = state) do
    {:noreply, %{state | mode: SWatch}}
  end

  def handle_info(:"top-left", %{mode: SWatch, ui_pid: ui, time: time, st1: Working, st2: Idle} = state) do
    GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    {:noreply, %{state | mode: Time}}
  end

  def handle_info(:working, %{ui_pid: ui, mode: mode, st1: Working, time: time, alarm_time: alarm_time} = state) do
    Process.send_after(self(), :working, 1000)
    time = Time.add(time, 1)
    if mode == Time do
      GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:%S") })
    end

    if Time.truncate(time, :second) == Time.truncate(alarm_time, :second) do
      :gproc.send({:p, :l, :ui_event}, :start_alarm)
    end

    {:noreply, state |> Map.put(:time, time) }
  end

  #Edicion del tiempo, pregunta 4
  def handle_info(:"bottom-right", %{mode: Time, st2: Idle} = state) do
    Process.send_after(self(), :waiting_to_editing, 250)
    {:noreply, %{state | st2: Waiting}}
  end

  def handle_info(:"bottom-right", %{mode: Time, st2: Waiting} = state) do
    {:noreply, %{state | st2: Idle}}
  end

  def handle_info(:waiting_to_editing, %{st2: Waiting} = state) do
    timer = Process.send_after(self(), :editing_to_editing, 250)

    :gproc.send({:p, :l, :ui_event}, :stop_clock)
    {:noreply, %{state | st1: Stopped, st2: Editing, count: 0, selection: Hour, show: true, timer: timer}}
  end

  def handle_info(:stop_clock, %{st1: Working} = state) do
    {:noreply, %{state | mode: TEditing, st1: Stopped}}
  end

  def handle_info(:resume_clock, %{st1: Stopped} = state) do
    Process.send_after(self(), :working, 1000)
    {:noreply, %{state | mode: Time, st1: Working}}
  end

  def handle_info(:editing_to_editing, %{ui_pid: ui, time: time, st2: Editing, count: cuenta, show: show, selection: selection, timer: timer} = state) do
    if timer != nil do
      Process.cancel_timer(timer)
    end
    cuenta = cuenta + 1

    case selection do
      Hour ->
        if show do
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:%S") })
        else
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "  :%M:%S") })
        end

      Minute ->
        if show do
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:%S") })
        else
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:  :%S") })
        end

      _ ->
        if show do
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:%S") })
        else
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:  ") })
        end
    end

    if cuenta < 20 do
      timer = Process.send_after(self(), :editing_to_editing, 250)
      {:noreply, %{state | st1: Stopped, st2: Editing, count: cuenta, show: !show, timer: timer}}
    else
      :gproc.send({:p, :l, :ui_event}, :resume_clock)
      {:noreply, %{state | st2: Idle}}
    end
  end

   #Change Selection, edicion del tiempo
   def handle_info(:"bottom-right", %{st2: Editing, selection: selection} = state) do
    timer = Process.send_after(self(), :editing_to_editing, 250)
    {:noreply, %{state | count: 0, show: true, timer: timer,
      selection: case selection do
        Hour -> Minute
        Minute -> Second
        _ -> Hour
        end
    }}
  end

  #Increase Selection, edicion del tiempo
  def handle_info(:"bottom-left", %{st2: Editing, time: time, selection: selection} = state) do
    timer = Process.send_after(self(), :editing_to_editing, 250)
    time = case selection do
      Hour -> Time.add(time, 3600, :second)
      Minute -> Time.add(time, 60, :second)
      _ -> Time.add(time, 1, :second)
    end
    {:noreply, %{state | time: time, count: 0, timer: timer}}
  end

  #Edicion de Alarma
  def handle_info(:"bottom-left", %{mode: Time, st2: Idle} = state) do
    time = Time.new!(0,0,0)
    Process.send_after(self(), :waiting_to_editing_alarm, 250)
    {:noreply, %{state | st2: WaitingAlarm, alarm_time: time}}
  end

  def handle_info(:"bottom-left", %{mode: Time, st2: WaitingAlarm} = state) do
    {:noreply, %{state | st2: Idle}}
  end

  def handle_info(:waiting_to_editing_alarm, %{st2: WaitingAlarm} = state) do
    timer = Process.send_after(self(), :editingalarm_to_editingalarm, 250)

    :gproc.send({:p, :l, :ui_event}, :stop_clock)
    {:noreply, %{state | st1: Stopped, st2: EditingAlarm, count: 0, selection: Hour, show: true, timer: timer}}
  end

  def handle_info(:editingalarm_to_editingalarm, %{ui_pid: ui, alarm_time: time, st2: EditingAlarm, count: cuenta, show: show, selection: selection, timer: timer} = state) do
    if timer != nil do
      Process.cancel_timer(timer)
    end
    cuenta = cuenta + 1

    case selection do
      Hour ->
        if show do
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:%S") })
        else
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "  :%M:%S") })
        end

      Minute ->
        if show do
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:%S") })
        else
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:  :%S") })
        end

      _ ->
        if show do
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:%S") })
        else
          GenServer.cast(ui, {:set_time_display, Calendar.strftime(time, "%c", preferred_datetime: "%H:%M:  ") })
        end
    end


    if cuenta < 20 do
      timer = Process.send_after(self(), :editingalarm_to_editingalarm, 250)
      {:noreply, %{state | st1: Stopped, st2: EditingAlarm, count: cuenta, show: !show, timer: timer}}
    else
      :gproc.send({:p, :l, :ui_event}, :resume_clock)
      {:noreply, %{state | st2: Idle}}
    end
  end

  #Change Selection, edicion de alarma
  def handle_info(:"bottom-right", %{st2: EditingAlarm, selection: selection} = state) do
    timer = Process.send_after(self(), :editingalarm_to_editingalarm, 250)
    {:noreply, %{state | count: 0, show: true, timer: timer,
      selection: case selection do
        Hour -> Minute
        Minute -> Second
        _ -> Hour
        end
    }}
  end

  #Increase Selection, edicion de alarma
  def handle_info(:"bottom-left", %{st2: EditingAlarm, alarm_time: time, selection: selection} = state) do
    timer = Process.send_after(self(), :editing_to_editing, 250)
    time = case selection do
      Hour -> Time.add(time, 3600, :second)
      Minute -> Time.add(time, 60, :second)
      _ -> Time.add(time, 1, :second)
    end
    {:noreply, %{state | alarm_time: time, count: 0, timer: timer}}
  end


  def handle_info(_event, state), do: {:noreply, state}
end
